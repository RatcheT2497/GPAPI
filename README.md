**Generic Binary Parsing API**

Developed for ComputerCraft by Dan200.

Build requirements: 

- GCC
- Automake

Build instructions:

- Run `make` in the main project directory.
- Result should be in the `out` folder.