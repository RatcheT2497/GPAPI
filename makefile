cc = gcc
src = src/main.lua
out = out/gpapi.lua
flags = -x c -Iinclude
$(out): $(src)
	$(cc) -E -P $(flags) $< -o $@
.PHONY: $(out)