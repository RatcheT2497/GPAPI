#ifndef _GPAPI_LUA_
#define _GPAPI_LUA_
--[[
-------- GPAPI
---- Copyright (c) 2018 RatcheT2498
---- MIT License
---- Permission is hereby granted, free of charge, to any person obtaining a copy
---- of this software and associated documentation files (the "Software"), to deal
---- in the Software without restriction, including without limitation the rights
---- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
---- copies of the Software, and to permit persons to whom the Software is
---- furnished to do so, subject to the following conditions:
---- 
---- The above copyright notice and this permission notice shall be included in all
---- copies or substantial portions of the Software.
---- 
---- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
---- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
---- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
---- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
---- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
---- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
---- SOFTWARE.
--]]
local m_floor, m_ceil = math.floor, math.ceil
local b_lsh, b_or, b_rsh, b_and, b_xor = bit.blshift, bit.bor, bit.brshift, bit.band, bit.bxor
local s_char = string.char

#ifdef _GPAPI_DEFINES_
	#define BIN_UINT8 	(0)
	#define BIN_UINT16 	(1)
	#define BIN_UINT24 	(2)
	#define BIN_UINT32 	(3)
	#define BIN_SINT8 	(4)
	#define BIN_SINT16 	(5)
	#define BIN_SINT24 	(6)
	#define BIN_SINT32 	(7)
	#define BIN_STRING	(8)
	local _bin_read_type = function(t, valtype)
		if not t[0] then t[0]=1 end
		if t[0] > #t then return nil end
		local val
		if valtype < 8 then
			val=0
			local bnum = (valtype % 4) + 1
			local s = m_floor(valtype / 4)
			local temp = 0
			for i = 1, bnum do
				local b=t[t[0]]
				val = b_or(val, b_lsh(b, (i-1)*8))
				t[0]=t[0]+1
			end
			if s == 1 then
				local bnum = ((valtype % 4)+1)*8
				local _val = 1
				for i = 0, bnum-2 do
					_val = b_or(b_lsh(val, 1), 1)
				end
				local lt = b_xor(b_lsh(1, bnum-1), _val)
				if val < lt then val = val + (_val + 1) end
			end
		else
			val=""
			while (t[t[0]] ~= 0) or (t[0] < #t) do
				val=val .. s_char(t[t[0]])
				t[0]=t[0]+1
			end
		end
		return val
	end
	local bin_transform_break = function(names, vtype)
		return function(r, v)
			local bnum = ((vtype % 4) + 1) * 8
			local ret = {}
			for i = 0, bnum-1 do
				local v=b_rsh(b_and(v, b_lsh(1, i)), i)
				if names[i+1] ~= nil then
					ret[names[i+1]] = ((v == 1) and true or false)
				end
			end
			return ret
		end;
	end
#else
	local _bin_number = {
		["u8"] = 1; ["s8"] = 1;
		["u16"] = 2; ["s16"] = 2;
		["u24"] = 3; ["s24"] = 3;
		["u32"] = 4; ["s32"] = 4;
	}
	local _bin_read_type = function(t, valtype)
		if not t[0] then t[0]=1 end
		if t[0] > #t then return nil end
		valtype = valtype:lower()
		local val
		if valtype ~= "str" then
			val=0
			local bnum = _bin_number[valtype]
			local s = (valtype:sub(1,1) == "s")
			local temp = 0
			for i = 1, bnum do
				local b=t[t[0]]
				val = b_or(val, b_lsh(b, (i-1)*8))
				t[0]=t[0]+1
			end
			if s == 1 then
				bnum=bnum*8
				local _val = 1
				for i = 0, bnum-2 do
					_val = b_or(b_lsh(val, 1), 1)
				end
				local lt = b_xor(b_lsh(1, bnum-1), _val)
				if val < lt then val = val + (_val + 1) end
			end
		else
			val=""
			while (t[t[0]] ~= 0) or (t[0] < #t) do
				val=val .. s_char(t[t[0]])
				t[0]=t[0]+1
			end
		end
		return val
	end
	local bin_transform_break = function(names, valtype)
		return function(r, v)
			local bnum = _bin_number[valtype] * 8
			local ret = {}
			for i = 0, bnum-1 do
				local v=b_rsh(b_and(v, b_lsh(1, i)), i)
				if names[i+1] ~= nil then
					ret[names[i+1]] = ((v == 1) and true or false)
				end
			end
			return ret
		end;
	end
#endif
local bin_transform_truth_check = function(t, err, ret)
	if ret == nil then ret = true end
	return function(r, v)
		if #t ~= #v then error("1: " .. err .. "(" .. #t .. " ~= " .. #v .. ")") end
		for i = 1, #t do
			if v[i] ~= t[i] then
				error("2: " .. err .. textutils.serialize(v) .. ", " .. textutils.serialize(t));
			end
		end
		if ret then return v end
	end
end
local bin_transform_cut_linear = function(n)
	return function(r, v)
		local tab = {}
		for i = 1, m_floor(#v/n) do
			local _t={}
			for z = 1, n do
				_t[#_t+1]=v[((i-1)*n)+z]
			end
			tab[#tab+1] = _t
		end
		return tab
	end
end

local bin_get_bits = function(byte, bcount)
	if byte == nil then return {}; end
	local ret = {}
	for i = 0, bcount-1 do
		local v = b_rsh(b_and(byte, b_lsh(1, i)), i)
		ret[#ret+1] = ((v == 1) and true or false)
	end
	return ret
end

/*
local data = {
	{condition:function/nil, name:string/number, type:number, count:number/string/function, transform:function}
	...
}
*/
local _bin_get_value = function(ret, v)
	if type(v) == "string" then
		if type(ret[v]) ~= "number" then
			error("GPAPI: Invalid value type!")
		end
		return ret[v];
	end
	return v;
end
local _bin_add_table = function(ret, data)
	local op = data[1];
	if #data < 3 then error("GPAPI: Invalid operands!") end
	if (op <= BIN_OP_FIRST) or (op >= BIN_OP_LAST) then error("GPAPI: Invalid operation!") end
	local o1, o2 = _bin_get_value(ret, data[2]), _bin_get_value(ret, data[3])
	if type(o1) == "table" then o1 = _bin_add_table(ret, o1) end
	if type(o2) == "table" then o2 = _bin_add_table(ret, o2) end
	if op == BIN_OP_ADD then return (o1 + o2)
	elseif op == BIN_OP_SUB then return (o1 - o2)
	elseif op == BIN_OP_MUL then return (o1 * o2)
	elseif op == BIN_OP_DIV then return (o1 / o2) 
	end
end

local bin_parse = function(t, data)
	local ret = {}
	for i = 1, #data do
		local d = data[i]
		local val
		local dcond = d[1]
		local cond = true
		if dcond then cond = dcond(ret, t) end
		if cond then
			local dname = d[2]
			local dtype = d[3]
			local dcount = _bin_get_value(ret, d[4])
			if type(dcount) == "function" then
				dcount = dcount(ret, t);
			elseif type(dcount) == "table" then
				dcount = _bin_add_table(ret, dcount)
			end
			local dtransform = d[5]		
			if not dcount then
				error("GPAPI: Tried reading past end of table!")
			end

			if dcount > 1 then 
				-- Read multiple
				val = {} 
				for z = 1, dcount do
					val[#val+1]=_bin_read_type(t, dtype)
				end
			elseif dcount == 1 then
				-- Read single
				val = _bin_read_type(t, dtype)
			elseif dcount < 0 then
				-- Read multiple, from end
				val = {}
				while t[0] <= #t+(dcount+1) do
					val[#val+1]=_bin_read_type(t, dtype)
				end
			elseif dcount == 0 then
				-- Read custom
				val = dtransform(ret, t)
			end
			if dtransform and dcount ~= 0 then
				val = dtransform(ret, val)
			end
			if type(dname) == "string" then
				if #dname > 0 then
					ret[dname] = val
				end
			else
				ret[dname] = val
			end
		end
	end
	return ret
end
#ifdef _GPAPI_RELEASE_
return {
	parse = bin_parse;
	transform_truth_check = bin_transform_truth_check;
	transform_cut_linear = bin_transform_cut_linear;
	transform_break = bin_transform_break;
	get_bits = bin_get_bits;
}
#endif
#endif